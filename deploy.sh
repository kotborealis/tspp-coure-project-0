#! /usr/bin/env bash

export SSHPASS=$SSH_PASS
sshpass -e scp -o StrictHostKeyChecking=no -P $SSH_PORT \
    ./build/expressionSolver-linux-x86 $SSH_USER@$SSH_HOST:/data/
sshpass -e scp -o StrictHostKeyChecking=no -P $SSH_PORT \
    -r ./bot_wrapper/* $SSH_USER@$SSH_HOST:/data/
sshpass -e ssh -n -oStrictHostKeyChecking=no -p $SSH_PORT $SSH_USER@$SSH_HOST "\
	chmod +x /data/expressionSolver-linux-x86; \
	cd /data/; \
    export EXPR_SOLVER=/data/expressionSolver-linux-x86 \
        CALC_BOT_TOKEN=$CALC_BOT_TOKEN; \
	bash ./startup.sh;"