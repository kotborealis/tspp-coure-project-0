from Bot.BotMain import executor
from aiogram import Dispatcher

adminIdList = [
    '141366148',          # Maxim
    '218214537'
]


async def shutdown(dp: Dispatcher):
    await dp.storage.close()
    await dp.storage.wait_closed()


async def send_to_team(dp: Dispatcher):
    for adminId in adminIdList:
        try:
            un = await dp.bot.me
            await dp.bot.send_message(chat_id=adminId,
                                      text=str.format('Бот {0} запущен | Это сообщение видят только админы',
                                                      un['username']))
        except:
            pass


if __name__ == "__main__":
    from Bot.Handlers.MessageHandlers import *
    from Bot.Handlers.InlineQueryHandlers import *

    executor.on_shutdown(shutdown)
    executor.on_startup(send_to_team)
    executor.start_polling()
