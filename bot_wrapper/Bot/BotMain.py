from aiogram.utils.executor import Executor
from aiogram import Bot
from aiogram import Dispatcher
from Bot.ShuntingYardProxy import ShuntingYardProxy

from configparser import ConfigParser

import os

bot = Bot(token=os.environ['CALC_BOT_TOKEN'])
dispatcher = Dispatcher(bot=bot)
executor = Executor(dispatcher)

calc_proxy = ShuntingYardProxy(calling_name=os.environ['EXPR_SOLVER'])

