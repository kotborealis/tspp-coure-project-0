from Bot import messages
from Bot.BotMain import dispatcher, calc_proxy
from aiogram.types import ParseMode
from aiogram import types

bot = dispatcher.bot


@dispatcher.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.answer(text=messages.start_text,
                         parse_mode=ParseMode.MARKDOWN)


@dispatcher.message_handler(commands=['help'])
async def process_help_command(message: types.Message):
    await message.answer(text=messages.help_text,
                         parse_mode=ParseMode.MARKDOWN)


@dispatcher.message_handler(lambda message: True)
async def process_user_expression(message: types.Message):
    code, res = calc_proxy.get_expression_result(message.text.strip())
    output_text = res
    if code == 1:
        output_text = messages.invalid_expression
    elif code == 2:
        output_text = messages.critical_error
        # log error
    await message.answer(text=output_text,
                         parse_mode=ParseMode.MARKDOWN)


@dispatcher.message_handler(lambda message: True)
async def reply_message(message: types.Message):
    print("NO HANDLER:")
    print(message)

