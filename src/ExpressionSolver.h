#pragma once
#include <string>
#include <vector>
#include <map>
#include <functional>


enum class TokenType {
	Number = 0,
	Operation,
	LeftBracket,
	RightBracket,
	Invalid,
};

class Token {
public:
	Token(const TokenType type);
	TokenType type();
	virtual std::string stringValue() = 0;
protected:
	TokenType _type;
};

class NumberToken : public Token {
public:
	NumberToken(const std::string& num);
	std::string stringValue() override;

	double value();
private:
	std::string _str;
	//double _num;
};

enum class Associativity {
	Left,
	Right
};

class OperationToken : public Token {
public:
	OperationToken(const char op);
	std::string stringValue() override;
	unsigned priority();
	Associativity associativity();
private:
	unsigned _opPriority;
	Associativity _associativity;
	char	 _op;
};

class BracketToken : public Token {
public:
	BracketToken(const char br);
	std::string stringValue() override;
private:
	char _br;
};

class ExpressionSolver {
public:
	ExpressionSolver() {}

	static double solve(const std::string& expression);
private:
	static std::vector<Token*> tokenize(std::string expression);
	static std::vector<Token*> shuntingYard(const std::vector<Token*>& tokens);
	static double calculate(std::vector<Token*> tokens);
};

