#include "ExpressionSolver.h"

#include <stdexcept>
#include <algorithm>
#include <cmath>

#include <stack>
#include <vector>


Token::Token(const TokenType type) : _type(type) {}

TokenType Token::type() {
	return _type;
}

NumberToken::NumberToken(const std::string& num) : Token(TokenType::Number), _str(num) {}

std::string NumberToken::stringValue() {
	return _str;
}

double NumberToken::value() {
	return std::stod(_str);
}

OperationToken::OperationToken(const char op) : Token(TokenType::Operation), _op(op) {
	switch (_op) {
	case '+':
	case '-': {
		_opPriority = 1;
		_associativity = Associativity::Left;
		break;
	}
	case '*':
	case '/': {
		_opPriority = 2;
		_associativity = Associativity::Left;
		break;
	}
	case '^': {
		_opPriority = 3;
		_associativity = Associativity::Right;
		break;
	}
	default:
		throw std::runtime_error("������������ ������ ���������");
	}
}

std::string OperationToken::stringValue() {
	return std::string(1, _op);
}

unsigned OperationToken::priority()
{
	return _opPriority;
}

Associativity OperationToken::associativity()
{
	return _associativity;
}

BracketToken::BracketToken(const char br) : Token(TokenType::Invalid) {
	switch (br) {
	case '(': {
		_type = TokenType::LeftBracket;
		break;
	}
	case ')': {
		_type = TokenType::RightBracket;
		break;
	}
	default:
		throw std::runtime_error("Error in bracket-init");
	}
	_br = br;
}

std::string BracketToken::stringValue() {
	return std::string(1, _br);
}

double ExpressionSolver::solve(const std::string& expression) {
	auto tokens = tokenize(expression);
	return calculate(shuntingYard(tokens));
}

std::vector<Token*> ExpressionSolver::tokenize(std::string expression) {
	expression.erase(std::remove(expression.begin(), expression.end(), ' '), expression.end());
	std::vector<Token*> tokens;
	size_t currentIdx = 0;
	auto prevSymbol = '\0';

	auto isUnary = [&currentIdx, &prevSymbol, &expression]() {
		if (expression[currentIdx] == '-' && (prevSymbol == '\0' || prevSymbol == '(')) {
			prevSymbol = expression[currentIdx];
			++currentIdx;
			return true;
		}
		return false;
	};

	auto readNumber = [&currentIdx, &prevSymbol, &expression](bool isUnaryMinus) -> NumberToken* {
		std::string number = isUnaryMinus ? "-" : "";
		if ('0' <= expression[currentIdx] && expression[currentIdx] <= '9') {
			bool delimeterFlag = false;
			while ('0' <= expression[currentIdx] && expression[currentIdx] <= '9'
				||(!delimeterFlag && expression[currentIdx] == '.')) 
			{
				if (expression[currentIdx] == '.') {
					delimeterFlag = true;
				}
				number.push_back(expression[currentIdx]);
				prevSymbol = expression[currentIdx];
				++currentIdx;
			}
			return new NumberToken(number);
		}
		return nullptr;
	};

	auto readOperations = [&currentIdx, &prevSymbol, &expression]() -> OperationToken* {
		std::string op = "";
		if (expression[currentIdx] == '+'
			|| expression[currentIdx] == '-'
			|| expression[currentIdx] == '*'
			|| expression[currentIdx] == '/'
			|| expression[currentIdx] == '^')
		{
			prevSymbol = expression[currentIdx];
			++currentIdx;
			return new OperationToken(expression[currentIdx - 1]);
		}
		return nullptr;
	};

	auto readBrackets = [&currentIdx, &prevSymbol, &expression]() -> BracketToken* {
		std::string br = "";
		if (expression[currentIdx] == '('
			|| expression[currentIdx] == ')')
		{
			prevSymbol = expression[currentIdx];
			++currentIdx;
			return new BracketToken(expression[currentIdx - 1]);
		}
		return nullptr;
	};

	while (currentIdx < expression.size()) {
		auto bufIdx = currentIdx;
		bool isUnaryMinus = isUnary();
		auto numToken = readNumber(isUnaryMinus);
		if (numToken) {
			tokens.push_back(numToken);
		}
		auto opToken = readOperations();
		if (opToken) {
			tokens.push_back(opToken);
		}
		auto brToken = readBrackets();
		if (brToken) {
			tokens.push_back(brToken);
		}
		if (bufIdx == currentIdx) {
			throw std::runtime_error("Error token in " + std::to_string(currentIdx));
		}
	}
	return tokens;
}

std::vector<Token*> ExpressionSolver::shuntingYard(const std::vector<Token*>& tokens) {
	std::vector<Token*> output;
	std::stack<Token*> opStack;

	for (auto token : tokens) {
		if (token->type() == TokenType::Number) {
			output.push_back(token);
			continue;
		}
		else if (token->type() == TokenType::Operation) {
			auto currentOp = dynamic_cast<OperationToken*>(token);
			while (!opStack.empty() && opStack.top()->type() != TokenType::LeftBracket) {
				auto op = dynamic_cast<OperationToken*>(opStack.top());
				if (!(op->priority() > currentOp->priority()
					|| (op->priority() == currentOp->priority()
						&& currentOp->associativity() == Associativity::Left))) {
					break;
				}
				output.push_back(op);
				opStack.pop();
			}
			opStack.push(token);
		}
		else if (token->type() == TokenType::LeftBracket) {
			opStack.push(token);
		}
		else if (token->type() == TokenType::RightBracket) {
			while (!opStack.empty() && opStack.top()->type() != TokenType::LeftBracket) {
				output.push_back(opStack.top());
				opStack.pop();
			}
			if (opStack.empty()) {
				throw std::runtime_error("Mismatched brackets");
			}
			opStack.pop();
		}
	}
	while (!opStack.empty()) {
		if (opStack.top()->type() == TokenType::LeftBracket
			|| opStack.top()->type() == TokenType::RightBracket)
		{
			throw std::runtime_error("Mismatched brackets");
		}
		output.push_back(opStack.top());
		opStack.pop();
	}

	return output;
}

double ExpressionSolver::calculate(std::vector<Token*> tokens) {
	std::stack<double> bufStack;

	auto popTop = [&bufStack]() -> double {
		if (bufStack.empty()) {
			throw std::runtime_error("������������ ���������: ��������� �� ������� ���������");
		}
		auto res = bufStack.top();
		bufStack.pop();
		return res;
	};

	for (auto token : tokens) {
		if (token->type() == TokenType::Number) {
			bufStack.push((dynamic_cast<NumberToken*>(token)->value()));
		}
		if (token->type() == TokenType::Operation) {
			auto op = dynamic_cast<OperationToken*>(token);
			auto first = popTop();
			auto second = popTop();

			if (op->stringValue() == "+") {
				bufStack.push(second + first);
			}
			else if (op->stringValue() == "-") {
				bufStack.push(second - first);
			}
			else if (op->stringValue() == "*") {
				bufStack.push(second * first);
			}
			else if (op->stringValue() == "/") {
				bufStack.push(second / first);
			}
			else if (op->stringValue() == "^") {
				bufStack.push(std::pow(second, first));
			}
		}
	}

	return bufStack.top();
}
