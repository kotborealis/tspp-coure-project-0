#define CATCH_CONFIG_MAIN 
#include "ExpressionSolver.h"
#include "catch.hpp"

TEST_CASE("Simple operations") {
	SECTION("Sum") {
		REQUIRE(ExpressionSolver::solve("2+2") == 4);
		REQUIRE(ExpressionSolver::solve("2+0") == 2);
		REQUIRE(ExpressionSolver::solve("2+(2+2)") == 6);
	}
}

TEST_CASE("Order") {
	SECTION("Mul") {
		REQUIRE(ExpressionSolver::solve("2*(2+2)") == 8);
		REQUIRE(ExpressionSolver::solve("2*2+2") == 6);
	}
}